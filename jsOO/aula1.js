function Person() {
	this.name = '';
	this.age = '';
	this.eyesColor = '';
	this.body = '';

	this.move = function(){

	};

	this.say = function(){
		console.log("Hello World!");
	};

	this.see = function(){

	};

}

var user = new Person();

console.log(typeof Person);
console.log(typeof user);

user.name = 'user';
user.age = 20;
user.eyesColor = 'black';
user.body = 'thin';

console.log(user);
user.say();

var user2 = new Person();

user2.name = 'user2';
user2.age = 40;
user2.eyesColor = 'white';
user2.body = 'fat';

console.log(user2);