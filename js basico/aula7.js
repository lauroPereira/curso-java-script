document.write("<h1>Aula 7</h1>");
document.write("<h2>metodo concat</h2>");
document.write("<p>Array 1 antes: " + arr1);
arr1 = arr1.concat(arr2);
document.write("<p>Array 1 concatenado ao Array 2: " + arr1);

document.write("<h2>metodo Join</h2>");
var list = arr1.join(" - ");
document.write("<p>Array 1 com o metodo Join usando hifen como separador: " + list);

document.write("<h2>metodo Push</h2>");
var list = arr3.join(" - ");
document.write("<p>Array 3 antes do push: " + list);
arr3.push("Novo Elemento");
var list = arr3.join(" - ");
document.write("<p>Array 3 após do push: " + list);

document.write("<h2>metodo Pop</h2>");
var list = arr3.join(" - ");
document.write("<p>Array 3 antes do pop: " + list);
arr3.pop();
var list = arr3.join(" - ");
document.write("<p>Array 3 após do pop: " + list);

document.write("<h2>metodo Shift</h2>");
arr3.push("Novo elemento");
var list = arr3.join(" - ");
document.write("<p>Array 3 antes do Shift: " + list);
arr3.shift();
var list = arr3.join(" - ");
document.write("<p>Array 3 após do Shift: " + list);