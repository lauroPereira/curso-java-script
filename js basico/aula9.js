document.write("<h1>Aula 9</h1>");
document.write("<h2>metodo Sort</h2>");
var arr4 = new Array(5, 9, 6, 2, 1, 8, 4, 7, 3);
var list = arr4.join(" - ");
document.write("<p>Array 4 antes do Sort: " + list);
arr4.sort();
var list = arr4.join(" - ");
document.write("<p>Array 4 após o Sort: " + list);

//Utilizando callback
function desc(a,b){
	if(a > b){
		return -1;
	}
	if(a < b){
		return 1;
	}
	if(a === b){
		return 0;
	}
}

document.write("<h2>metodo Sort utilizando callback</h2>");
var arr4 = new Array(5, 9, 6, 2, 1, 8, 4, 7, 3);
var list = arr4.join(" - ");
document.write("<p>Array 4 antes do Sort(desc): " + list);
arr4.sort(desc);
var list = arr4.join(" - ");
document.write("<p>Array 4 após o Sort(desc): " + list);