document.write("<h1>Aula 10</h1>");
arr5 = new Array("orange", "blue", "red", "green", "yellow", "blue");
document.write("<h2>Metodo indexOf</h2>");
var list = arr5.join(" - ");
document.write("<p>Array 5: " + list);

document.write("<h3>indexOf('blue')</h3>")
var ret = arr5.indexOf("blue");
document.write("<p>Resultado: " + ret);
document.write("<p>Valor: " + arr5[ret]);

document.write("<h3>indexOf('b')</h3>")
var ret = arr5.indexOf("b");
document.write("<p>Resultado: " + ret);
document.write("<p>Valor: " + arr5[ret]);

document.write("<h3>indexOf('blue', 2)</h3>")
var ret = arr5.indexOf("blue", 2);
document.write("<p>Resultado: " + ret);
document.write("<p>Valor: " + arr5[ret]);

document.write("<h2>Metodo lastIndexOf</h2>");
document.write("<h3>lastIndexOf('blue')</h3>")
var ret = arr5.lastIndexOf("blue");
document.write("<p>Resultado: " + ret);
document.write("<p>Valor: " + arr5[ret]);

document.write("<h3>lastIndexOf('blue', 2)</h3>")
var ret = arr5.lastIndexOf("blue", 2);
document.write("<p>Resultado: " + ret);
document.write("<p>Valor: " + arr5[ret]);