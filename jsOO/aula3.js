function Person(){
	var name = '';
	var age = 0;
	
	this.getName = function() {
		return this.name;
	};
	this.setName = function(_name) {
		this.name = _name;
	};

	this.getAge = function() {
		return this.age;
	};
	this.setAge = function(_age) {
		this.age = _age;
	};
}

var teste = new Person();

console.log(teste);

teste.setName('Tester');
teste.setAge(1000);

console.log(teste.getName() + " - " + teste.getAge());