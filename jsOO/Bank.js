function Bank() {
	var name = '';
	var account = '';
	var amount = 0;

	this.setName = function(_name){
		name = _name
	};

	this.getName = function(){
		return name;
	};

	this.setAccount = function(_account){
		account = _account;
	};

	this.getAccount = function(){
		return account;
	};

	this.setAmount = function(_amount){
		amount = _amount;
	};

	this.getAmount = function(){
		return amount;
	};

}

function BankStrategy(_strategy, _bank){
	this.strategy = _strategy;
	this.bank = _bank;
}

BankStrategy.prototype.doStrategy = function(){
	return this.strategy();
}

var deposit = function(bank, value){
	var amount = bank.getAmount() + value;

	window.console.log('Deposito realizado no valor de R$' + value);
	window.console.log('Seu saldo atual é de R$' + amount);
}

var withdraw = function(bank, value){
	var amount = bank.getAmount() - value;

	window.console.log('Retirada realizada no valor de R$' + value);
	window.console.log('Seu saldo atual é de R$' + amount);
}

function Main() {
	var bank = new Bank();

	var name = window.prompt('Nome do banco:');
	var acc = window.prompt('Conta:');
	var amount = parseInt(window.prompt('Valor:'));

	bank.setName(name);
	bank.setAccount(acc);
	bank.setAmount(amount);

	console.log(bank.getName() + ' - ' + bank.getAccount() + ' - R$' + bank.getAmount());

	var option = 0;

	do{
		var option = parseInt(window.prompt('Escolha um opção: \n1 - Depósito\n2 - Retirada\n0 - Sair'));
		var val = 0;
		switch(option){
			case 1:
				val = parseInt(window.prompt('Quanto deseja depositar?'));

				new BankStrategy(deposit(bank,val));
			break;
			case 2:
				val = parseInt(window.prompt('Quanto deseja retirar?'));

				new BankStrategy(withdraw(bank,val));
			break;
			case 0:
				window.console.log('Sistema finalizado!');
			break;
			default:
				window.console.log('Opção inválida!');
			break;
		}
	}while(option !== 0);
}

Main();