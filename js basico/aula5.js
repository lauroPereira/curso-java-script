var arr1 = new Array("red", 10, 20.4);
var arr2 = Array("blue", 12, 30.3, arr1);
var arr3 = ["orange", 10, 23.2, arr1, arr2];

document.write("<p>Array 1: " + arr1[0] + "</p>");
document.write("<p>Array 1: " + arr1[1] + "</p>");
document.write("<p>Array 1: " + arr1[2] + "</p>");

document.write("<p>Array 2: " + arr2[0] + "</p>");
document.write("<p>Array 2: " + arr2[1] + "</p>");
document.write("<p>Array 2: " + arr2[2] + "</p>");
document.write("<p>Array 2: " + arr2[3] + "</p>");

document.write("<p>Array 3: " + arr3[0] + "</p>");
document.write("<p>Array 3: " + arr3[1] + "</p>");
document.write("<p>Array 3: " + arr3[2] + "</p>");
document.write("<p>Array 3: " + arr3[3] + "</p>");
document.write("<p>Array 3: " + arr3[4] + "</p>");