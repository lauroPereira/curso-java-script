document.write("<h1>Aula 8</h1>");
document.write("<h2>metodo Unshift</h2>");
var list = arr3.join(" - ");
document.write("<p>Array 3 antes do Unshift: " + list);
arr3.unshift("green");
var list = arr3.join(" - ");
document.write("<p>Array 3 após do Unshift: " + list);

document.write("<h2>metodo Slice</h2>");
var newArray = arr1.slice(2,4);
var list = newArray.join(" - ");
document.write("<p>Novo array pegando slice(2,4) do Array 1: " + list);

document.write("<h2>metodo Splice</h2>");
var list = arr1.join(" - ");
document.write("<p>Array 1 antes do Splice: " + list);
arr1.splice(1,2,"A","B","C","D");
var list = arr1.join(" - ");
document.write("<p>Array 1 após splice(1,2,'A','B','C','D'): " + list);

document.write("<h2>metodo Reverse</h2>");
var list = arr1.join(" - ");
document.write("<p>Array 1 antes do Reverse: " + list);
arr1.reverse();
var list = arr1.join(" - ");
document.write("<p>Array 1 após Reverse: " + list);